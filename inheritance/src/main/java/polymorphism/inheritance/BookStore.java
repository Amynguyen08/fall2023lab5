package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args){
        Book[] books = new Book[5];
        books[0] = new Book("Book On Dinos", "Michael");
        books[2] = new Book("All About Cats", "Amy");
        books[1] = new ElectronicBook("All About Dogs ", "Bianca", 5);
        books[3] = new ElectronicBook("How to Cook", "Ramsey", 8);
        books[4] = new ElectronicBook("How to Read", "Venus P.", 10);
        
        for(Book b: books){
            System.out.println(b);
        }

        ElectronicBook b = (ElectronicBook)books[0];
        System.out.println(b.getNumberBytes());
    }
}
