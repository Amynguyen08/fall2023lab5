package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }

    @Override
    public String toString(){
        String fromBase = super.toString();
        fromBase += ", Number of bytes: " + this.numberBytes;
        return fromBase;
    }
}
